json.array! @questions do |question|
  json.(question,
    :id, 
    :title, 
    :description, 
    :created_at, 
    :updated_at)
  json.user question.user, :id, :username, :profile_picture
  
end

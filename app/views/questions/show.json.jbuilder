json.extract! @question, :id, :created_at, :updated_at, :title, :description
json.user @question.user, :id, :username, :profile_picture

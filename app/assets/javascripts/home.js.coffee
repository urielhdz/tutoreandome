# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "ready page:load", ->
	$.material.init()
	@myIcons = new SVGMorpheus('#open_user_data svg',
		duration: 200
	)
	@

	angular.bootstrap(document.body, ['mainModule'])

	$("#open_user_data").on "click", =>
		if $("#user-data").data("open") == "true"
			@myIcons.to("menu_icon",{"easing": "linear"})
			$("#user-data").data("open","false")
			$("#user-data").animate
				left: -$("#user-data").width()
			200
		else
			@myIcons.to("close_icon",{"easing": "linear"})
			$("#user-data").data("open","true")
			$("#user-data").animate
				left: 0
			200
	
	$(".snackbar").on "click", ->
		$(this).fadeOut()
	nav_height = $("#main-nav").height()
	$("#user-data").animate
		height: window.screen.availHeight - nav_height
		top: nav_height

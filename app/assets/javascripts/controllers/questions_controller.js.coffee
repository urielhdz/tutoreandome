angular.module("mainModule").controller "QuestionsCtrl", ($scope,$resource)->
	Question = $resource("/questions.json/:id", {id: "@id"}, {update: {method: "PUT"}})
	$scope.newQuestion = {}
	$scope.formVisible = false
	$scope.activeQuestion = {}
	$scope.myIconQuestion = new SVGMorpheus('.open_question_form svg',
		duration: 200
	)
	$scope.setActiveQuestion = (question)->
		if $scope.activeQuestion == question
			$scope.activeQuestion = {}
		else
			$scope.activeQuestion = question
	$scope.addQuestion = ->
		question = Question.save($scope.newQuestion, (data)->
			console.log data
		,(error)->
			console.log error
		)
		$scope.questions.unshift(question)
		$scope.newQuestion = {}
		$scope.formVisible = false
		return false
	$scope.$watch (scope)->
		scope.formVisible
	,(newValue,oldValue)->
		if newValue
			$scope.myIconQuestion.to("close_icon",{"easing": "linear"})
		else
			$scope.myIconQuestion.to("live_help_icon",{"easing": "linear"})
	$scope.toggleForm = ->
		if $scope.formVisible
			$scope.formVisible = false 
		else
			$scope.formVisible = true
	$scope.closeForm = ->
		$scope.formVisible = false
	$scope.questions = Question.query()
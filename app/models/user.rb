class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable,
		:recoverable, :rememberable, :trackable, :validatable,:omniauthable, :omniauth_providers => [:facebook,:google_oauth2]
	after_create :build_username
	has_many :questions

    def self.from_omniauth(auth)
		where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
			user.email = auth.info.email
			user.password = Devise.friendly_token[0,20]
			user.name = auth.info.name   # assuming the user model has a name
		end
	end
	def profile_picture
		if self.provider == "facebook"
			"http://graph.facebook.com/#{self.uid}/picture?width=140"
		elsif self.provider == "google_oauth2" and self.profile_picture_url
			self.profile_picture_url[0...-2] + "300"
		else
			"/default.jpg"
		end
	end
	
	def store_pp_google
		if self.provider == "google_oauth2"
			uri = URI.parse("https://www.googleapis.com/plus/v1/people/#{self.uid}?fields=image&key=#{ENV['google_api_key']}")
		    http = Net::HTTP.new(uri.host, uri.port)
			http.use_ssl = true
			http.verify_mode = OpenSSL::SSL::VERIFY_NONE
			request = Net::HTTP::Get.new(uri.request_uri)
		    response = http.request(request)
		    js = JSON.parse(response.body)
		    if js.has_key?("image")
		    	self.profile_picture_url = js["image"]["url"]
		    	self.save
		    end
		end
	end
	private
	def build_username
		self.username = Mail::Address.new(self.email).local
		self.save
	end
end

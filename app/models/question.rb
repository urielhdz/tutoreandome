class Question < ActiveRecord::Base
  belongs_to :user
  validates :title, presence: true
  validates :description, presence: true
  validates_length_of :description, minimum: 20
end

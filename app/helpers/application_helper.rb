module ApplicationHelper
	def render_svg(path)
	    File.open("public/#{path}", "rb") do |file|
	    	raw file.read
	  	end
	end
	def render_profile_picture(user_pp,size=100)
		"<div style='width:#{size}px;height:#{size}px;background:url(#{user_pp});background-size:cover;border-radius:50%;background-position:center;'></div>"
	end
end
